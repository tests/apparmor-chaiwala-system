#!/bin/bash
#	Copyright (C) 2002-2005 Novell/SUSE
#	Copyright (C) 2018 Collabora Ltd.
#
#	This program is free software; you can redistribute it and/or
#	modify it under the terms of the GNU General Public License as
#	published by the Free Software Foundation, version 2 of the
#	License.

#=NAME exec
#=DESCRIPTION Runs exec() through ux, ix & px functionality

pwd=`dirname $0`
pwd=`cd $pwd ; /bin/pwd`

bin=$pwd

# RETAIN from the calling script is being passed in $1
. $bin/prologue.inc

OK=0
NO_SUCH_FILE=2
PERM_DENIED=13

CURRENT_APPLICATION=home

read_ok=/tmp/${CURRENT_APPLICATION}/Storage/chaiwala/foo
read_ko=/tmp/${CURRENT_APPLICATION}/Storage/foo

# Those won't be cleaned up if for any reason runchecktest fails :-(
initialize() {
	mkdir -p `dirname $read_ok`
	touch $read_ok
	mkdir -p `dirname $read_ko`
	touch $read_ko
}

initialize
# Check that the tunable and the USER layout is composed properly (See Application proposal)
settest read ${TESTBINDIR}/read
genprofile abstraction:chaiwala-base \
	variable:CURRENT_APPLICATION=${CURRENT_APPLICATION} \
        /tmp/@{CURRENT_APPLICATION}/Storage/*/foo:r
runchecktest "try to open $read_ok (ok)" pass $OK $read_ok
runchecktest "try to open $read_ko (with no permission)" pass $PERM_DENIED $read_ko

rm -f $read_ok $read_ko 2> /dev/null 2> /dev/null
