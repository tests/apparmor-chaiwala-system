#!/bin/bash
#	Copyright (C) 2002-2005 Novell/SUSE
#	Copyright (C) 2018 Collabora Ltd.
#
#	This program is free software; you can redistribute it and/or
#	modify it under the terms of the GNU General Public License as
#	published by the Free Software Foundation, version 2 of the
#	License.

#=NAME exec
#=DESCRIPTION Runs exec() through ux, ix & px functionality

pwd=`dirname $0`
pwd=`cd $pwd ; /bin/pwd`

bin=$pwd

# RETAIN from the calling script is being passed in $1
. $bin/prologue.inc

settest exec ${TESTBINDIR}/exec

# it should be a not overly simple binary, otherwise inheritance of "Pix" will let it be allowed
usr_binary_no_profile="/bin/ls"
usr_binary_no_profile_ARGS="${TESTBINDIR}/exec"
usr_binary_profile="/bin/ping"
usr_binary_profile_ARGS="-c1 localhost"
lib_binary_no_profile=/usr/lib/ssl/misc/c_issuer
lib_binary_profile=/usr/lib/telepathy/telepathy-gabble

# 'ls' is provided by coreutils
coreutils=`which coreutils`
# 'ping' is provided by busybox
busybox=`which busybox`

# Generate a profile including chaiwala-base + permission to exec $usr_binary_no_profile
genprofile abstraction:chaiwala-base "$usr_binary_no_profile:Pix" "$coreutils:Pix"
# $usr_binary_no_profile passes for the inherit rule (Pix), since it can exec
# the binary and access ARGS.
runchecktest "EXEC $usr_binary_no_profile (no profile, but inherit)" pass $usr_binary_no_profile $usr_binary_no_profile_ARGS
# $usr_binary_no_profile failes for the inherit rule (Pix), since it cannot
# access ARGV1 (root dir). It fails because of resource access, not because the transition
# fails.
runchecktest "EXEC $usr_binary_no_profile (no profile, no inherit)" fail $usr_binary_no_profile /

# Generate a profile for $usr_binary_profile (/bin/ping).
# NOTE: We need to pass -T to disable tunables because tunables are automatically
# included with abstractions, and that leads to a duplicate include, and an
# AppArmor parser error.
genprofile abstraction:chaiwala-base "$usr_binary_profile:Pix" "$busybox:Pix" -- image="$busybox" abstraction:nameservice network:raw capability:net_raw -T
runchecktest "EXEC $usr_binary_profile (profiled)" pass $usr_binary_profile $usr_binary_profile_ARGS
runchecktest "EXEC $lib_binary_no_profile (no profile, out of traditional bin paths)" fail $lib_binary_no_profile
runchecktest "EXEC $lib_binary_profile (profiled, out of traditional bin paths)" fail $lib_binary_profile

genprofile abstraction:chaiwala-base "$usr_binary_no_profile:Pix" "$coreutils:Pix" -- image="$coreutils" /:r /*:r /proc/**/maps:r /tmp/apparmor-chaiwala-system*/*:rw
runchecktest "EXEC $usr_binary_no_profile with sibling profile (profiled and profiles allows resource)" pass $usr_binary_no_profile /
