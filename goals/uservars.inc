# 1. Path to apparmor parser
subdomain=/sbin/apparmor_parser

# 2. additional arguments to the apparmor parser
parser_args="-q -K"

# 3. template directory to be used for temp files (prologue.inc will add a suffix to it)
# Need to be able to access this directory by the root and nobody users.
tmpdir="$(mktemp -d /tmp/apparmor-chaiwala-system-XXXXXXXXXX)"

# 4. Location of load system profiles for verification
sys_profiles=/sys/kernel/security/apparmor/profiles
