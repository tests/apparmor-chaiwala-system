#!/bin/sh

# Copyright: 2012 Collabora ltd.
# Author: Cosimo Alfarano <cosimo.alfarano@collabora.co.uk>

# Run all tests, mainly to be user under Lava, with a custom parser

# a list of .sh which are not actually a test, so we can silently ignore
NOT_TESTS="env_check.sh"
# test that we need to skip, and let LAVA know about it
SKIP_TESTS=""

TESTDIR=$(cd $(dirname $0); pwd; cd - >/dev/null 2>&1)
. "${TESTDIR}/config.sh"
. "${TESTDIR}/common/update-test-path"

PREFIX="${TESTDIR}/goals"

case `uname -m` in
x86_64) TESTBINDIR=${TESTDIR}/amd64/bin; ;;
armv7l) TESTBINDIR=${TESTDIR}/armhf/bin; ;;
aarch64) TESTBINDIR=${TESTDIR}/arm64/bin; ;;
esac
export TESTBINDIR

num_failed=0

if [ "$(whoami)" != "root" ]; then
    exec sudo -E $0
fi

export RESULTDIR=""
unset VERBOSE
RETAIN=-r

get_only_results_dir() {
	tail -n 1 | sed "s/Files retained in:.\(.*\)/\1/g"
}

cleanup() {
  rm -f $TMP
}

TMP=$(mktemp)
for TEST in ${PREFIX}/*.sh; do
  unset skip
  for AVOID_TEST in ${SKIP_TESTS}; do
    if [ "$(basename ${TEST} .sh)" = "$(basename ${AVOID_TEST} .sh)" ]; then
      echo ${TEST}: SKIP -
      skip=true
    fi
  done
  for AVOID_TEST in ${NOT_TESTS}; do
    if [ "$(basename ${TEST} .sh)" = "$(basename ${AVOID_TEST} .sh)" ]; then
      # we don't need to notify LAVA about this skip.
      skip=true
    fi
  done
  test -n "$skip" && continue

  sh ${TEST} ${RETAIN} 2>&1 | tee $TMP
  OUTCOME=$?
  RESULTDIR=$(cat $TMP | get_only_results_dir)

  if [ "$OUTCOME" != 0 ]; then
        echo ${TEST}: FAILED - $RESULTDIR
        num_failed=$(($num_failed + 1))
  else
        echo ${TEST}: PASSED - $RESULTDIR
  fi
done
cleanup
exit $num_failed
